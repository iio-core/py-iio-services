from __future__ import unicode_literals

import json
import uuid
import inspect
import types
from threading import Thread


class ThreadWithReturnValue(Thread):
    """ Custom Thread where join method returns the result """
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, *, daemon=None):
        Thread.__init__(self, group, target, name, args, kwargs, daemon=daemon)
        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args, **self._kwargs)

    def join(self):
        Thread.join(self)
        return self._return


class JsonDumpDict(dict):
    """ Specialized dict class in order to properly dump json string """
    def __str__(self):
        return json.dumps(self)


def getMethodNames(obj):
    """ Returns all methods from obj """
    return [n for n, v in inspect.getmembers(obj, inspect.ismethod) if isinstance(v, types.MethodType)]


def UUID():
    """ Returns UUID v4 """
    return uuid.uuid4()
