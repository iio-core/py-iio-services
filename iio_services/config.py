from __future__ import unicode_literals

import tempfile
import logging
import socket
import os


def getConfiguration():
    # Get IP adress
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    hostAddress = s.getsockname()[0]
    s.close()
    # Manage environment variables
    if 'NODE_ENV' in os.environ.keys() and os.environ.get('NODE_ENV') == 'production':
        IIOS_SERVER_PORT = 8080
    else:
        IIOS_SERVER_PORT = int(os.environ.get('IIOS_SERVER_PORT')) if 'IIOS_SERVER_PORT' in os.environ.keys() else 4093
    IIOS_NAMESPACE = os.environ.get('IIOS_NAMESPACE') if 'IIOS_NAMESPACE' in os.environ.keys() else 'hyperasset'
    IIOS_SERVER_HOST = os.environ.get('IIOS_SERVER_HOST') if 'IIOS_SERVER_HOST' in os.environ.keys() else hostAddress
    IIOS_SERVER_PATH_TO_SERVE = os.environ.get('IIOS_SERVER_PATH_TO_SERVE') if 'IIOS_SERVER_PATH_TO_SERVE' in os.environ.keys() else 'static/service'
    IIOS_LOG_DIR = os.environ.get('IIOS_LOG_DIR') if 'IIOS_LOG_DIR' in os.environ.keys() else tempfile.gettempdir()
    IIOS_LOG_LEVEL = os.environ.get('IIOS_LOG_LEVEL') if 'IIOS_LOG_LEVEL' in os.environ.keys() else 'DEBUG'
    if IIOS_LOG_LEVEL in logging._nameToLevel:
        IIOS_LOG_LEVEL = logging._nameToLevel[IIOS_LOG_LEVEL]
    else:
        IIOS_LOG_LEVEL = logging.DEBUG
    REDIS_HOST = os.environ.get('REDIS_HOST') if 'REDIS_HOST' in os.environ.keys() else '127.0.0.1'
    REDIS_PORT = int(os.environ.get('REDIS_PORT')) if 'REDIS_PORT' in os.environ.keys() else 6379
    REDIS_DB = int(os.environ.get('REDIS_DB')) if 'REDIS_DB' in os.environ.keys() else 0
    REST_API_KEY = os.environ.get('REST_API_KEY') if 'REST_API_KEY' in os.environ.keys() else '2bpukqziosbejet2k9duvpadajsfrm4u'
    REST_API_CONTEXT = os.environ.get('REST_API_CONTEXT') if 'REST_API_CONTEXT' in os.environ.keys() else 'api'
    return {
        'namespace': IIOS_NAMESPACE,
        'logs': {
            'logDir': IIOS_LOG_DIR,
            'logLevel': IIOS_LOG_LEVEL,
        },
        'redis': {
            'host': REDIS_HOST,
            'port': REDIS_PORT,
            'db': REDIS_DB,
        },
        'server': {
            'host': IIOS_SERVER_HOST,
            'port': IIOS_SERVER_PORT,
            'path': IIOS_SERVER_PATH_TO_SERVE,
        },
        'rest': {
            'context': REST_API_CONTEXT,
            'apiKeys': [REST_API_KEY],
        },
        'publicOptions': {
            'accessRights': {
                'owner': 'admin',
                'group': 'admin',
                'access': {
                    'owner': 'rw',
                    'group': 'rw',
                    'all': 'rw',
                }
            },
            'uiComponentInjection': True,
            'description': {
                # To be defined by service
            }
        }
    }
