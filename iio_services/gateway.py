from __future__ import unicode_literals

import json
import time
from redis.client import Redis
from multiprocessing import Process, Queue
from .utils import utils
from .service import Service
from .config import getConfiguration


class Gateway(Service):

    def __init__(self):
        """ Gateway Service Constructor """
        if not hasattr(self, 'options'):
            self.options = getConfiguration()
        if self.options.get('name') is None:
            self.options['name'] = '__gateway'
        super().__init__()
        self.services = super()._getAvailableNSServices()
        for service in self.services:
            self._addService(self.services[service])
        self._startService()

    def _getServiceInfo(self, name):
        """ Get service informations, None if service does not exist """
        service = None
        try:
            conn = Redis(connection_pool=self.redis_pool)
            service = conn.get(self.namespace + ':' + name)
            if service is not None:
                tmp_json = service.decode('utf8')
                serviceJSON = json.loads(tmp_json)
                return serviceJSON
        except Exception as ex:
            pass
        return service

    def _waitForService(self, name, timeout):
        """ Wait for remote service name to be available (= in the dico). None if timeout. """
        def _waitForServiceTimeout(obj, name, queue):
            while 1:
                if name in obj.services:
                    queue.put(True)
                    break
                time.sleep(0.5)

        result = None
        queue = Queue()
        proc = Process(target=_waitForServiceTimeout, args=(self, name, queue,))
        proc.start()
        try:
            result = queue.get(timeout=timeout)
            proc.join()
        except Exception as ex:
            proc.terminate()
        return result

    def _addService(self, service):
        """ Add service """
        if service is None or 'name' not in service:
            return
        self.services[service['name']] = {}
        for methodName in service['methods']:
            self._addMethod(service['name'], methodName)
        # New service available and registered event
        Service.eventBus.emit('service:registered', service)

    def _threadCallServiceMethod(self, req, userID, serviceName, methodName):
        """ Main service method call """
        def generateListener(funcName, queue):
            def onMessage(obj, channel, message):
                if message is not None and 'req' not in message and 'token' in message and message['token'] == token:
                    queue.put(message['res'])
            onMessage.__name__ = funcName
            return onMessage

        try:
            token = str(utils.UUID())
            channel = '{}:{}:{}'.format(self.namespace, serviceName, methodName)
        except Exception as ex:
            return {'error': str(ex)}

        # Subscribe to channel in order to get response message
        self._subscribe(channel)

        queue = Queue()
        messageCallback = generateListener('onMessage_{}'.format(token), queue)
        self.eventBus.add_event(messageCallback, 'message')

        reqMessage = {
            'origin': str(self.uuid),
            'req': req,
            'token': token,
            'userId': req['userId'] if 'userId' in req else userID
        }
        conn = Redis(connection_pool=self.redis_pool)
        conn.publish(channel, reqMessage)

        result = None
        timeout = self.options['timeout'] if 'timeout' in self.options else 5
        try:
            result = queue.get(timeout=timeout)
        except Exception as ex:
            result = {'error': 'timeout'}

        Service.eventBus.remove_event('onMessage_{}'.format(token), 'message')
        return result

    def _addMethod(self, serviceName, methodName):
        """ Add service method processing """
        def _callServiceMethod(req, userID):
            """ Callback function for service method call """
            if req is None:
                req = {}
            threadRequest = utils.ThreadWithReturnValue(target=self._threadCallServiceMethod, args=(req, userID, serviceName, methodName,))
            threadRequest.daemon = True
            threadRequest.start()
            return threadRequest
        self.services[serviceName][methodName] = _callServiceMethod

    @Service.eventBus.on(event='service:down')
    def _onServiceDown(self, service):
        """ Callback on service down """
        self.services.pop(service, None)

    @Service.eventBus.on(event='service:up')
    def _onServiceUp(self, service):
        """ Callback on service up """
        self._addService(service)
