from __future__ import unicode_literals

import os
import sys
import json
import time
import redis
import logging
import threading
from logging.handlers import RotatingFileHandler
from event_bus import EventBus
from redis.client import Redis
from .utils import utils
from .config import getConfiguration


class Service:
    """ Base class for IIO Service """
    HTTP_METHODS = ["GET", "POST", "PUT", "PATCH", "DELETE"]
    eventBus = EventBus()

    def __init__(self):
        """ Service Constructor """
        self.redis_pool = None
        self.subscriberLock = threading.Lock()
        if not hasattr(self, 'options'):
            self.options = getConfiguration()
        self.uuid = utils.UUID()
        self.namespace = 'iios:' + self.options.get('namespace') if self.options.get('namespace') is not None else 'iios:ds'
        self.name = self.options.get('name') if self.options.get('name') is not None else str(self.uuid)
        self.redisDB = self.options.get('redis').get('db') if self.options.get('redis').get('db') is not None else 0
        # Methods that can be called remotely for this service
        self.methods = None
        # Current subscription channels
        self.subscriptions = []
        # Current subscribed methods indexed on corresponding channel
        self.subscribedMethods = {}
        # Current events subscriptions
        self.subscribedEvents = {}
        # Logger file + console
        self.logger = logging.getLogger()
        self.logger.setLevel(self.options.get('logs').get('logLevel'))
        formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
        fileHandler = RotatingFileHandler(os.path.join(self.options.get('logs').get('logDir'), '{}.log'.format(self.__class__.__name__)),
                                          'a', 10000000, 5)
        fileHandler.setLevel(self.options.get('logs').get('logLevel'))
        fileHandler.setFormatter(formatter)
        self.logger.addHandler(fileHandler)
        streamHandler = logging.StreamHandler()
        streamHandler.setLevel(self.options.get('logs').get('logLevel'))
        self.logger.addHandler(streamHandler)

        meta = {}
        meta['port'] = self.options['server']['port']
        meta['host'] = self.options['server']['host']
        meta['rest'] = self.options['rest']
        self.options['publicOptions']['metadata'] = meta

    def _startService(self):
        """ Start service """
        self.logger.info('---------------------------------------')
        self.logger.info('Starting {} service...'.format(self.__class__.__name__))
        self.logger.info('---------------------------------------')
        # REDIS management
        if self.redis_pool is None:
            self.redis_pool = redis.BlockingConnectionPool(host=self.options.get('redis').get('host'),
                                                           port=self.options.get('redis').get('port'),
                                                           db=self.redisDB,
                                                           retry_on_timeout=True)
        self.redisSubscriberPubSub = Redis(connection_pool=self.redis_pool).pubsub()

        # Activate All Keyspace events and keyEvent events
        conn = Redis(connection_pool=self.redis_pool)
        conn.config_set('notify-keyspace-events', 'KEA')

        # Launch thread monitoring and register public methods
        self.threadMonitor = threading.Thread(target=self._monitorMessages)
        self.threadMonitor.daemon = True
        self.threadMonitor.start()
        self._registerMethods()

    def _callServiceMethod(self, channel, methodName, request):
        """ Call registered REDIS service function """
        resMessage = {
            'origin': str(self.uuid)
        }
        try:
            parameters = json.loads(request)
            resMessage['token'] = parameters['token']
            func = getattr(self, methodName)
            resMessage['res'] = func(parameters['req'])
        except Exception as ex:
            resMessage['serviceErr'] = str(ex)

        conn = Redis(connection_pool=self.redis_pool)
        conn.publish(channel, utils.JsonDumpDict(resMessage))

    def _getHTTPMethodType(self, methodName):
        """ Returns HTTP method type and exported public function name """
        upperMethodName = methodName.upper()
        for HTTPMethod in Service.HTTP_METHODS:
            if upperMethodName.startswith(HTTPMethod):
                resMethodName = methodName[len(HTTPMethod):]
                return HTTPMethod, resMethodName.lower()
        # Default: export method as HTTP GET request with same name
        return "GET", methodName

    def _registerOneMethod(self, methodName):
        """ Register a public method to REDIS and create REST callback """
        # Subscribe to method related channel
        exportedMethodType, exportedMethodName = self._getHTTPMethodType(methodName)
        channel = self.namespace + ':' + self.name + ':' + exportedMethodName
        self._subscribe(channel)
        self.subscriptions.append(channel)
        self.subscribedMethods[channel] = {
            'function': self._callServiceMethod,
            'method': methodName,
            'type': exportedMethodType,
            'exported_name': exportedMethodName,
        }
        return exportedMethodName

    def _registerMethods(self):
        """ Registered public methods available from remote """
        # Clear existing subscriptions if any (due to multiple register calls)
        if len(self.subscriptions) > 0:
            self._unsubscribe(self.subscriptions)
        # Automatically detects methods
        self.methods = []
        publicMethods = [method for method in utils.getMethodNames(self) if not method.startswith("_")]
        for method in publicMethods:
            exportedMethodName = self._registerOneMethod(method)
            self.methods.append(exportedMethodName)

        # Add service to local and remote dico
        serviceInfo = {
            'name': self.name,
            'type': 'native',
            'methods': self.methods,
            'options': self.options['publicOptions']
        }
        conn = Redis(connection_pool=self.redis_pool)
        conn.set(self.namespace + ':' + self.name, json.dumps(serviceInfo))

    def _getAvailableNSServices(self):
        """ Get all services available in current namespace """
        services = {}
        try:
            conn = Redis(connection_pool=self.redis_pool)
            for key in conn.scan_iter(self.namespace + ':*'):
                servicesList = conn.mget(key)
                for service in servicesList:
                    tmp_key = key.decode('utf8')
                    tmp_json = service.decode('utf8').replace("'", '"')
                    serviceJSON = json.loads(tmp_json)
                    shortName = tmp_key.replace(self.namespace + ':', '')
                    services[shortName] = serviceJSON
        except Exception as ex:
            services = {}
        return services

    def _monitorMessages(self):
        """ Manage incoming messages from REDIS """
        while True:
            try:
                self._psubscribe('__keyspace@' + str(self.redisDB) + '__:' + self.namespace + ':*')
                self._subscribe(self.namespace + '__event')
                while True:
                    message = self.redisSubscriberPubSub.get_message(timeout=1)
                    if message:
                        # Message type: 'subscribe', 'unsubscribe', 'psubscribe', 'punsubscribe', 'message', 'pmessage'
                        channel = message['channel'].decode('utf8') if 'channel' in message else ''
                        origin = name = ''
                        data_json = None
                        if message['type'] == 'message':
                            self.logger.debug('--- message: {}'.format(message))
                            try:
                                data = message['data'].decode('utf8').replace("'", '"') if 'data' in message else None
                                data_json = json.loads(data)
                                origin = data_json['origin'] if 'origin' in data_json else ''
                                name = data_json['name'] if 'name' in data_json else ''
                            except Exception as ex:
                                pass

                            if str(self.uuid) == origin:
                                continue
                            if '__event' in channel:
                                if name in self.subscribedEvents:
                                    for cb in self.subscribedEvents[name]:
                                        cb(data_json)
                                if '*' in self.subscribedEvents:
                                    for cb in self.subscribedEvents['*']:
                                        cb(data_json)
                            elif channel in self.subscribedMethods and data_json is not None:
                                realMethodName = self.subscribedMethods[channel]['method']
                                realFunction = self.subscribedMethods[channel]['function']
                                threadRequest = threading.Thread(target=realFunction, args=(channel, realMethodName, json.dumps(data_json)))
                                threadRequest.daemon = True
                                threadRequest.start()
                            Service.eventBus.emit('message', self, channel, data_json)

                        elif message['type'] == 'pmessage':
                            self.logger.debug('--- pmessage: {}'.format(message))
                            try:
                                pattern = message['pattern'].decode('utf8') if 'pattern' in message else ''
                                data = message['data'].decode('utf8').replace("'", '"') if 'data' in message else None
                                data_json = json.loads(data)
                                origin = data_json['origin'] if 'origin' in data_json else ''
                                name = data_json['name'] if 'name' in data_json else ''
                            except Exception as ex:
                                pass

                            if '__event' in channel:
                                if str(self.uuid) == origin:
                                    continue
                                if name in self.subscribedEvents:
                                    for cb in self.subscribedEvents[name]:
                                        cb(data_json)
                            elif channel in self.subscribedMethods and data_json is not None:
                                if str(self.uuid) == origin:
                                    continue
                                realMethodName = self.subscribedMethods[channel]['method']
                                realFunction = self.subscribedMethods[channel]['function']
                                threadRequest = threading.Thread(target=realFunction, args=(channel, realMethodName, json.dumps(data_json)))
                                threadRequest.daemon = True
                                threadRequest.start()
                            else:
                                serviceFullName = channel.replace('__keyspace@' + str(self.redisDB) + '__:', '')
                                serviceShortName = serviceFullName.replace(self.namespace + ':', '')
                                conn = Redis(connection_pool=self.redis_pool)
                                service = conn.get(serviceFullName)
                                if service is not None:
                                    tmp_json = service.decode('utf8')
                                    serviceJSON = json.loads(tmp_json)
                                    service = serviceJSON
                                else:
                                    service = serviceShortName

                                if data is not None:
                                    if data == 'set':
                                        Service.eventBus.emit('service:up', self, service)
                                    elif data == 'del':
                                        Service.eventBus.emit('service:down', self, service)
                            Service.eventBus.emit('pmessage', self, pattern, channel, data_json)

                        else:
                            self.logger.debug('--- Other: {}'.format(message))

            except Exception as ex:
                self.logger.error('GATEWAY ERROR: {}'.format(ex))

            # Manage Redis exception
            self.redis_pool.disconnect()
            self.redis_pool.reset()
            while True:
                try:
                    conn = Redis(connection_pool=self.redis_pool)
                    conn.ping()
                    self.redisSubscriberPubSub = Redis(connection_pool=self.redis_pool).pubsub()
                    # Activate All Keyspace events and keyEvent events
                    conn = Redis(connection_pool=self.redis_pool)
                    conn.config_set('notify-keyspace-events', 'KEA')
                    # Register public methods
                    self._registerMethods()
                    break
                except Exception as ex:
                    self.logger.error('GATEWAY ERROR: {}'.format(ex))
                    time.sleep(3)

    def _subscribePushEvent(self, name, cb):
        """ Subscribes to service push event """
        if name not in self.subscribedEvents:
            self.subscribedEvents[name] = []
        self.subscribedEvents[name].append(cb)

    def _unsubscribePushEvent(self, name, cb):
        """ Unsubscribes to service push event """
        self._unsubscribe(self.namespace + '__event')
        self.subscribedEvents.pop(name, None)

    def _exitGracefully(self, signum, frame):
        """ SIGINT / SIGTERM handler """
        self._destroy()
        sys.exit(0)

    def _subscribe(self, channel):
        if self.redisSubscriberPubSub is not None:
            with self.subscriberLock:
                self.redisSubscriberPubSub.subscribe(channel)

    def _unsubscribe(self, channel):
        if self.redisSubscriberPubSub is not None:
            with self.subscriberLock:
                self.redisSubscriberPubSub.unsubscribe(channel)

    def _psubscribe(self, channel):
        if self.redisSubscriberPubSub is not None:
            with self.subscriberLock:
                self.redisSubscriberPubSub.psubscribe(channel)

    def _punsubscribe(self, channel):
        if self.redisSubscriberPubSub is not None:
            with self.subscriberLock:
                self.redisSubscriberPubSub.punsubscribe(channel)

    def _destroy(self):
        """ Destroy Service """
        self.logger.info('---------------------------------------')
        self.logger.info('Stopping {} service...'.format(self.__class__.__name__))
        try:
            conn = Redis(connection_pool=self.redis_pool)
            conn.delete(self.namespace + ':' + self.name)
            self._unsubscribe(self.namespace + '__event')
            self._punsubscribe('__keyspace@' + self.redisDB + '__:' + self.namespace + ':*')
        except Exception as ex:
            pass
        self.logger.info('---------------------------------------')

    def _getRedisPool(self):
        return self._redis_pool

    def _setRedisPool(self, value):
        self._redis_pool = value

    redis_pool = property(_getRedisPool, _setRedisPool)
