# PYTHON IIO services implementation

Basic service discovery and gateway for both PUB/SUB and HTTP RPC emulation.

## 1. Installation

### 1.1 You need to install python (version >= 3.5), pip and virtualenv on your machine.
You also must have cloned the git repository and checkout the correct branch before starting the next steps.

### 1.2 Make a virtual environment

If not installed, install virtualenv

 ```
    pip install virtualenv
 ```

 Then create it:

```
    cd <path to py-iio-services sources>
    virtualenv env -p python3
```

Of course, please replace `path to py-iio-services sources` with the path to wherever you put your local py-iio-services repository.
For example: 'C:\dev\py-iio-services'. If you have several Python versions installed, please also make sure the Python version used within this virtual environnement is Python >= 3.5.

This should create a virtual environment called 'env'. Every python/pip command that will be launched in the next sections will have to be run inside this virtual environnement. To do so, you can either activate it: all the python/pip commands will then be executed inside this environnement.

Activate the virtual env on Linux:

```
    cd <path to py-iio-services sources>
    source env/bin/activate
```


#### Remember to always work in your virtual environnement!

### 1.3 Install the packages.
When it is the case, the link is written as comment in requirements.txt

```
    pip install -r requirements.txt
```

## 1.4 Test

```bash
cd <path to py-iio-services sources>
python test/test.py
```

