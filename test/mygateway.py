from __future__ import unicode_literals

import signal
# Only for testing
import os
import sys
currentPath = os.path.abspath(".")
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
if currentPath not in sys.path:
    sys.path.insert(0, currentPath)
# ****
from iio_services.gateway import Gateway
from iio_services.config import getConfiguration


class MyGateway(Gateway):

    def __init__(self):
        self.options = getConfiguration()
        self.options['name'] = 'mygateway'
        signal.signal(signal.SIGINT, self._exitGracefully)
        signal.signal(signal.SIGTERM, self._exitGracefully)
        super().__init__()
