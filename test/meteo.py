from __future__ import unicode_literals

# Only for testing
import os
import sys
currentPath = os.path.abspath(".")
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)
if currentPath not in sys.path:
    sys.path.insert(0, currentPath)
# ****
import signal
import time
import json
import requests
from redis.client import Redis
from iio_services.service import Service
from iio_services.config import getConfiguration


class Meteo(Service):

    def __init__(self):
        """ Meteo Service Constructor """
        self.options = getConfiguration()
        self.options['name'] = 'meteo'
        self.options['publicOptions']['description'] = {
            'icon': 'assets/weather-64.png',
            'i18n': {
                'City': ['Localité'],
                'Weather forecast': ['Prévisions météo'],
                'Provides weather forecast for a given location': [
                    'Fournit les prévisions météo pour un endroit donné'
                ]
            },
            'title': 'Weather forecast',
            'info': 'Provides weather forecast for a given location'
        }
        self.options['publicOptions']['aspect'] = {
            'name': 'weather',
            'description': 'Weather information',
            'icon': 'assets/weather.png',
            'component': 'meteo'
        }
        super().__init__()
        signal.signal(signal.SIGINT, self._exitGracefully)
        signal.signal(signal.SIGTERM, self._exitGracefully)
        self._startService()

        # TODO: remove: only for testing
        time.sleep(1)
        """
        realMethodName = self.subscribedMethods['iios:hyperasset:meteo:location']['method']
        realFunction = self.subscribedMethods['iios:hyperasset:meteo:location']['function']
        requestJson = {
            "token": "MY-TOKEN",
            "req": {"city": "Lyon"}
        }
        realFunction('iios:hyperasset:meteo:location', realMethodName, json.dumps(requestJson))
        """

        requestJson = {
            "origin": "MOI!",
            "token": "MY-TOKEN",
            "req": {"city": "Lyon"},
            "userID": "TOTO"
        }
        conn = Redis(connection_pool=self.redis_pool)
        ret = conn.publish('iios:hyperasset:meteo:location', json.dumps(requestJson))
        print('Nb subscribers delivered: {}'.format(ret))

        while True:
            time.sleep(5)
        # ****

    def postLocation(self, request):
        """ POST location """
        if request is None or 'city' not in request:
            raise Exception('Missing required parameter: city')
        address = request['city']
        baseURL = 'http://nominatim.openstreetmap.org/search/'
        params = 'format=jsonv2'
        url = baseURL + address + '?' + params
        r = requests.post(url)
        if r.status_code != 200:
            raise Exception(r.text)
        response = r.json()
        if response[0]:
            return {
                'lat': response[0]['lat'],
                'lon': response[0]['lon'],
            }
        else:
            raise Exception('Not found')

    def postReverse(self, request):
        if request is None or 'lat' not in request or 'lon' not in request:
            raise Exception('Missing required parameters: lat/lon')
        baseURL = 'http://nominatim.openstreetmap.org/reverse?{}'
        params = 'format=jsonv2&lat={}&lon={}'.format(request['lat'], request['lon'])
        url = baseURL.format(params)
        r = requests.post(url)
        if r.status_code != 200:
            raise Exception(r.text)
        response = r.json()
        return {
            'location': {
                'lat': request['lat'],
                'lon': request['lon'],
            },
            'address': response['address'],
            'name': response['name'],
        }
