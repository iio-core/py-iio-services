from __future__ import unicode_literals

# import meteo
import mygateway
import time
from threading import Thread


# meteoService = meteo.Meteo()
gatewayService = mygateway.MyGateway()
# TODO: remove: only for testing
time.sleep(2)
# Get meteo service informations
if gatewayService._waitForService("meteo", 3) is not None:
    print(gatewayService._getServiceInfo('meteo'))

# Call meteo reverse function
gatewayService.services['meteo']['reverse']({"lat": 43.6118633, "lon": 6.6411581}, "UserID")


# Send requests to meteo service
def testMeteoService(index):
    threadRequest = gatewayService.services['meteo']['location']({"city": "Lyon"}, "UserID")
    result = threadRequest.join()
    print('{}: {}'.format(index, result))


for index in range(10):
    worker = Thread(target=testMeteoService, args=(index,))
    worker.setDaemon(True)
    worker.start()

"""
while True:
    time.sleep(5)
"""
# ****
